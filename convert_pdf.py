import base64
import os
import io
from datetime import datetime
from PIL import Image
from pdf2image import convert_from_path

filelist = [file for file in os.listdir('.') if file.endswith('.pdf')]
format_string = "%Y-%m-%dT%H:%M:%S.%f"

for pdffile in filelist:
    imagefiles = convert_from_path(pdffile,fmt='png')
    pagenum = 1
    for imagefile in imagefiles:
        filesize = len(pdffile)
        pngfile = pdffile[:filesize - 4] + "_page{0}.png".format(pagenum)
        xmlfile = pdffile[:filesize - 4] + "_page{0}.xml".format(pagenum)
        pagenum += 1
        
        imagefile.save(pngfile, format="png")
        encoded = base64.b64encode(open(pngfile, "rb").read()).decode('utf-8')

        width,height = imagefile.size

        modified = datetime.now().strftime(format_string)
    
        xmlfile = open(xmlfile, "w")

        xmlfile.write('<?xml version="1.0" encoding="UTF-8"?>')
        xmlfile.write('<mxfile host="app.diagrams.net" modified="{0}" agent="5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36" etag="asp1DOWi5hFHNRoAgojO" version="13.10.7">'.format(modified))
        xmlfile.write('  <diagram id="RPQaRYANeM_HLa-V3wZ5" name="Page-1">')
        xmlfile.write('    <mxGraphModel dx="1183" dy="741" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="{0}" pageHeight="{1}" math="0" shadow="0">'.format(width+500, height+500))
        xmlfile.write('      <root>')
        xmlfile.write('        <mxCell id="0" />')
        xmlfile.write('        <mxCell id="1" parent="0" />')
        xmlfile.write('        <mxCell id="K2ADjbHtVvqGTflGP8tr-1" value="" style="shape=image;verticalLabelPosition=bottom;labelBackgroundColor=#ffffff;verticalAlign=top;aspect=fixed;imageAspect=0;image=data:image/png,{0};" parent="1" vertex="1">'.format(encoded))
        xmlfile.write('          <mxGeometry x="60" y="60" width="{0}" height="{1}" as="geometry" />'.format(width, height))
        xmlfile.write('        </mxCell>')
        xmlfile.write('      </root>')
        xmlfile.write('    </mxGraphModel>')
        xmlfile.write('  </diagram>')
        xmlfile.write('</mxfile>')

        xmlfile.close()
        os.remove(pngfile)
